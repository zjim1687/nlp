import xml.etree.ElementTree as etree
from lxml import etree as etre
import codecs
import csv
import time
import os
import re
import math


PATH_WIKI_XML = './'#C:\\Users\\jeffh\\data\\'
#FILENAME_WIKI = 'wikiexample.txt'
FILENAME_WIKI = 'enwiki-20061130-pages-articles.xml'
FILENAME_TEXTS = 'enwiki_pre.xml'
#FILENAME_TEXTS = 'texts.xml'
ENCODING = "utf-8"


keyWords = {}
N = 0

def strip_tag_name(t):
    t = elem.tag
    idx = k = t.rfind("}")
    if idx != -1:
        t = t[idx + 1:]
    return t



def getKeys(s):
    return re.findall('\[\[(.*?)\]\]',s)

def gatherKeys(s, container):

    def buildDict(link):
        return {
            "link": link,
            "nw_key": 0,
            "nw": 0
        }

    for key in getKeys(s):
        splitedKey = key.split("|")

        if len(splitedKey) > 1:
            container[splitedKey[0].lower()] = buildDict(splitedKey[1])
        else:
            container[splitedKey[0].lower()] = buildDict(splitedKey[0])


pathWikiXML = os.path.join(PATH_WIKI_XML, FILENAME_WIKI)
pathTexts = os.path.join(PATH_WIKI_XML, FILENAME_TEXTS)

inrevision = False

print("Gathering keywords...")
for event, elem in etree.iterparse(pathWikiXML, events=('start', 'end')):
    
    tname = strip_tag_name(elem.tag)
    
    if event == 'start':
        if tname == 'page':
            inrevision = False
        elif tname == 'revision':
            # Do not pick up on revision id's
            inrevision = True
    else:
        #if tname == 'title':
        #    title = elem.text
        if tname == 'text' and inrevision:
            
            #print(elem.text)
            if elem.text is not None and len(elem.text) > 0:
                gatherKeys(elem.text, keyWords)

    elem.clear()


print("Gathering keyword informations...")
for event, elem in etree.iterparse(pathWikiXML, events=('start', 'end')):
    
    tname = strip_tag_name(elem.tag)
    
    if event == 'start':
        if tname == 'page':
            inrevision = False
        elif tname == 'revision':
            # Do not pick up on revision id's
            inrevision = True
    else:
        #if tname == 'title':
        #    title = elem.text
        if tname == 'text' and inrevision:
            
            #print(elem.text)
            if elem.text is not None and len(elem.text) > 0:
                for key, dict in keyWords.items():
                    lower_text = elem.text.lower()

                    if key in lower_text:
                        dict["nw"] += 1

                        if (lower_text.find('[[' + key) != -1):
                            dict["nw_key"] += 1
                
                N += 1
    elem.clear()

print ("Word post processing...")

root = etree.Element("root")
tree = etree.ElementTree(root)

for key, dict in keyWords.items():
    elem = etree.SubElement(root, 'word')

    ekey = etree.SubElement(elem, "key")
    ekey.text = key

    elink = etree.SubElement(elem, "link")
    elink.text = dict["link"]
    
    ekp = etree.SubElement(elem, "kp")
    ekp.text = str(dict['nw_key'] / dict['nw'])
    
    eidf = etree.SubElement(elem, "idf")
    eidf.text = str(math.log(N / dict['nw']))

tree.write(pathTexts)
