import xml.etree.ElementTree as etree
import codecs
import csv
import time
import os
import re
from nltk import ngrams


PATH_WIKI_XML = './'#C:\\Users\\jeffh\\data\\'
#FILENAME_TEXTS = '' #the path of the txt from where we read the text to wikify
FILENAME_TEXTS = 'test.txt'
#FILENAME_KEYS = 'texts.xml'
FILENAME_KEYS = 'enwiki_pre.xml'
FILENAME_KP_RESULT = 'kp_result.html'
FILENAME_TFIDF_RESULT = 'tfidf_result.html'
ENCODING = "utf-8"



def strip_tag_name(t):
    t = elem.tag
    idx = k = t.rfind("}")
    if idx != -1:
        t = t[idx + 1:]
    return t


def touple_to_str(touple):
    s=touple[0]
    for i in range(1,len(touple)):
        s += (' ' + touple[i])
    return s

def sort_by_tfidf(list):
    def sortFunc(e):
        return e["tfidf"]
    list.sort(key=sortFunc)
    
def sort_by_kp(list):
    def sortFunc(e):
        return e['core']['kp']
    list.sort(key=sortFunc)

def generate_link(word):
    w = re.sub(' ', '_', word)
    return 'https://en.wikipedia.org/wiki/' + w

def find_occurences_positions(string, substring):
    return [m.start() for m in re.finditer(substring, string)]


def generate_html(text, words):
    result = text
    for w in words:
        w_len = len(w['key'])

        for i in reversed(find_occurences_positions(result.lower(), w['key'])):
            result = result[:i] + '<a href="' + generate_link(w['core']['link']) + '">' + result[i:(i+w_len)] + '</a>' + result[(i + w_len):]
    
    return result

pathKeysXML = os.path.join(PATH_WIKI_XML, FILENAME_KEYS)
pathTexts = os.path.join(PATH_WIKI_XML, FILENAME_TEXTS)
pathKpResult = os.path.join(PATH_WIKI_XML, FILENAME_KP_RESULT)
pathTfResult = os.path.join(PATH_WIKI_XML, FILENAME_TFIDF_RESULT)

result = ""
words  = {}
actual = {}

print('Reading words...')
for event, elem in etree.iterparse(pathKeysXML, events=('start', 'end')):
    #tname = strip_tag_name(elem.tag)
    
    if event == 'start':
        if elem.tag == 'word':
            actual = {'value': {}} 
        elif elem.tag == 'key':
            actual['key'] = elem.text
        elif elem.tag == 'link':
            actual['value']['link'] = elem.text
        elif elem.tag == 'kp':
            actual['value']['kp'] = float(elem.text)
        elif elem.tag == 'idf':
            actual['value']['idf'] = float(elem.text)

    else:
        if elem.tag == 'word':
            words[actual['key']] = actual['value']

    elem.clear()

print('Words readen.')

print('Reading the text...')
f = open(pathTexts, "r", encoding=ENCODING)
source = f.read()
print('Text readen.')

print('Wikify...')
splited_source = source.split ()
wc = len (splited_source)
sixPercent = int (wc * 0.06)

grams_freq = {}

#print(words)
for n in range (1, 2):
    ngramok = ngrams(splited_source, n)

    for kif_touple in ngramok:
        
        kif = touple_to_str(kif_touple).lower()
        if kif in words:
            grams_freq[kif] = grams_freq.get(kif, 0) + 1
    
print('Wikify.ngramGathering ended.')

ordered_grams = [{'tfidf': tf*words[key]['idf'], 'core': words[key], 'key': key} for key, tf in grams_freq.items()]

print('Sorting by tfidf')
sort_by_tfidf(ordered_grams)
tfidf_file = open(pathTfResult, "w", encoding=ENCODING)
tfidf_file.write(generate_html(source, ordered_grams[:sixPercent+1]))
tfidf_file.close()

print('Sorting by Key phraseness')
sort_by_kp(ordered_grams)
Kp_file = open(pathKpResult, "w", encoding=ENCODING)
Kp_file.write(generate_html(source, ordered_grams[:sixPercent+1]))
Kp_file.close()

