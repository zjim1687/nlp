from keras.models import Model
from keras.layers import Input, Dense, Reshape, concatenate
from keras.layers.embeddings import Embedding
from keras.preprocessing.sequence import skipgrams
from keras.preprocessing import sequence
import tensorflow
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.metrics import mean_squared_error 
from numpy import dot
from numpy.linalg import norm
from tensorflow.keras import layers, models
import math

import urllib
import collections
import os
import zipfile

import numpy as np
import tensorflow as tf

def maybe_download(filename, url, expected_bytes):
    """Download a file if not present, and make sure it's the right size."""
    if not os.path.exists(filename):
        filename, _ = urllib.request.urlretrieve(url + filename, filename)
    statinfo = os.stat(filename)
    if statinfo.st_size == expected_bytes:
        print('Found and verified', filename)
    else:
        pass
        '''print(statinfo.st_size)
        raise Exception(
            'Failed to verify ' + filename + '. Can you get to it with a browser?')'''
    return filename


# Read the data into a list of strings.
def read_data(filename):
    """Extract the first file enclosed in a zip file as a list of words."""
    with zipfile.ZipFile(filename) as f:
        data = tf.compat.as_str(f.read(f.namelist()[0])).split()
    return data


def build_dataset(words, n_words, n_skip):
    """Process raw inputs into a dataset."""
    count = [['UNK', -1]]
    count.extend(collections.Counter(words).most_common(n_words + n_skip - 1)[n_skip:])
    dictionary = dict()
    for word, _ in count:
        dictionary[word] = len(dictionary)
    data = list()
    unk_count = 0
    for word in words:
        if word in dictionary:
            index = dictionary[word]
        else:
            index = 0  # dictionary['UNK']
            unk_count += 1
        data.append(index)
    count[0][1] = unk_count
    reversed_dictionary = dict(zip(dictionary.values(), dictionary.keys()))
    return data, count, dictionary, reversed_dictionary

def collect_data(vocabulary_size=10000, stop_size=50):
    url = 'http://mattmahoney.net/dc/'
    filename = maybe_download('text8.zip', url, 31344016)
    vocabulary = read_data(filename)
    print(vocabulary[:7])
    data, count, dictionary, reverse_dictionary = build_dataset(vocabulary,
                                                                vocabulary_size, stop_size)
    #del vocabulary  # Hint to reduce memory.
    return data, count, dictionary, reverse_dictionary, vocabulary

vocab_size = 10000
data, count, dictionary, reverse_dictionary, vocabulary = collect_data(vocabulary_size=vocab_size, stop_size=100)
print(count[:7])
print(data[:7])

window_size = 3
vector_dim = 100
epochs = 10


couples, labels = skipgrams(data, vocab_size, window_size=window_size)
#word_target, word_context = zip(*couples)
couples_np = np.array(couples, dtype="int32")
labels_np = np.array(labels, dtype="int32")

print(couples[:10], labels[:10])

positive_couples = couples_np[labels_np > 0.5]


keras = tensorflow.compat.v1.keras
Sequence = keras.utils.Sequence

class DataGenerator(Sequence):
    
    def __init__(self, dim, batch_size=100, shuffle=True):

        self.dim = dim
        self.n_batch = int(math.floor(len(positive_couples) / batch_size))
        self.shuffle = shuffle
        self.batch_size = batch_size
        self.on_epoch_end()

    def __len__(self):
        return self.n_batch

    def __getitem__(self, index):
        return self.get_batch(self.indexes[index])

    def on_epoch_end(self):
        self.indexes = np.arange(self.__len__())
        if self.shuffle == True:
            np.random.shuffle(self.indexes)

    def get_batch(self, batch_id):
        
        start_id = batch_id * self.batch_size
        end_id = min(start_id + self.batch_size, positive_couples.shape[0])
        actual_batch_size = end_id - start_id

        X = np.zeros((actual_batch_size, self.dim), dtype="int32")
        y = np.zeros((actual_batch_size, self.dim), dtype="int32")

        for i in range (actual_batch_size):
            X[i, int (positive_couples[i + start_id , 0])] = 1
            y[i, int (positive_couples[i + start_id , 1])] = 1
        return X, y


learn = False
if learn:

    training_generator = DataGenerator(vocab_size, 100, True)
    model = models.Sequential()

    model.add (layers.Input(shape=(vocab_size)))

    dense = layers.Dense (50)
    model.add (dense)

    model.add (layers.Dense(vocab_size, activation='softmax'))
    model.compile (optimizer='adam', loss='categorical_crossentropy')

    model.fit (training_generator, epochs=10, max_queue_size=10, workers=4, use_multiprocessing=False)

    weights = dense.get_weights ()[0]
    np.save ('emb_weights.npy', weights)

else:
    weights = np.load('emb_weights.npy')
    doc_len = 500
    voc_len = len(vocabulary)

    def get_doc(id):
        start_id = id * doc_len
        end_id = min(start_id+doc_len, voc_len)
        return vocabulary[start_id:end_id]

    def doc_count():
        return math.floor(voc_len / doc_len)

    def vect_cos (v1, v2):
        prod = norm (v1) * norm (v2)
        if (prod == 0.0):
            return 0.0
        return dot(v1, v2)/prod

    def bag_of_word(w1, w2):
        def tf(word, doc):
            return doc.count(word) / float(doc_len)
        
        def idf(word, docs):
            foundNum = 0.0
            for i in range(doc_count()):
                if word.lower() in get_doc(i):
                    foundNum += 1.0
    
            if foundNum > 0:
                return 1.0 + np.log(doc_count() / foundNum)
            else:
                return 1.0

        idf1 = idf (w1, vocabulary)
        idf2 = idf (w2, vocabulary)
        
        v1 = [tf(w1, get_doc(i)) * idf1 for i in range(doc_count())]
        v2 = [tf(w2, get_doc(i)) * idf2 for i in range(doc_count())]

        return vect_cos (v1, v2)

    def weight_sim (w1, w2):
        index1 = dictionary[w1]
        index2 = dictionary[w2]
        
        return  (vect_cos(weights[index1, :], weights[index2, :])+1) / 2

    bow_array = []
    my_array = []
    w_sim_array = []

    def compare_and_print(w1, w2, w_sim):
        bow = bag_of_word(w1, w2)
        #my = 0.0
        my  = weight_sim(w1, w2)

        print (w1, " ", w2, " ", w_sim, " ", bow, " ", my)

        bow_array.append(bow)
        my_array.append(my)
        w_sim_array.append(w_sim)

    print("word1 word2 word_sim bag_of_words my_result")
    
    foundPairs = 0
    with open('combined.csv') as file:
        line = file.readline ()
        while line:
            splited_row = line.split (',')
            
            if splited_row[0] in dictionary and splited_row[1] in dictionary:
                compare_and_print(splited_row[0], splited_row[1], float (splited_row[2]) / 10.0)
                foundPairs += 1

            line = file.readline()
        
    
    print("WordSim mse: ",mean_squared_error(w_sim_array, my_array))
    print("Bag of words mse: ",mean_squared_error(bow_array, w_sim_array))
    print(foundPairs, "pairs found")